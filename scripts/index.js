﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
(function () {
    "use strict";

    document.addEventListener( 'deviceready', onDeviceReady.bind( this ), false );

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener( 'pause', onPause.bind( this ), false );
        document.addEventListener( 'resume', onResume.bind( this ), false );
        
        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
        checkConnection();
        loadBrowser('http://google.com');
    };

    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };
    
    //user defined functions
    function checkConnection() {
        var networkState = navigator.connection.type;

        document.getElementById("info").innerHTML = 'Connection type: ' + networkState;
    }
    
    function loadBrowser(url) {
        
        var ref = window.open(url, '_blank', 'location=yes');
        ref.addEventListener('loadstart', function () { alert('start: ' + event.url); });
        ref.addEventListener('loadstop', function () { alert('stop: ' + event.url); });
        ref.addEventListener('exit', function () { alert(event.type); });
    }
    
    function checkBalance() {

        window.plugins.phoneDialer.dial('*124%23');
        /*phonedialer.dial("*124%23",
            function (err) {
                if (err == "empty")
                    alert("Unknown phone number");
                else
                    alert("Dialer Error:" + err);
            },
            function(success) {
                 alert(success);
            });*/
    }
} )();